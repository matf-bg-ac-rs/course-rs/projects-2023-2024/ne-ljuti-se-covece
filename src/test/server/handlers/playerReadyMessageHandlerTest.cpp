#include "../../catch.hpp"
#include "../../../common/message.h"
#include "../../../server/participants.h"
#include "../../../server/server.h"
#include "../../../server/handlers.h"
#include "../../../server/managers.h"

TEST_CASE("PlayerReadyHandlerTest", "[PlayerReadyHandlerTest]")
{
    SECTION("PlayerReadyHandlerTest : Uspesno slanje Ready poruke.")
    {
        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        serverThread->gameManager = Server::getInstance()->generateNewGame();
        serverThread->color = serverThread->gameManager->joinGame(serverThread);

        PlayerReadyMessage prm = PlayerReadyMessage();

        PlayerReadyMessageHandler* prmh = new PlayerReadyMessageHandler(new DefaultHandler());
        Response *response = prmh->handleMessage(&prm, serverThread);

        PlayerReadyResponse* prr = dynamic_cast<PlayerReadyResponse*>(response);
        REQUIRE(prr != nullptr);

        REQUIRE(prr->getSuccess() == true);
        REQUIRE(prr->getColor() == serverThread->color);
        REQUIRE(prr->getErrorMessage() == "");
    }

    SECTION("PlayerReadyHandlerTest : Niste povezani u partiju!")
    {
        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);

        PlayerReadyMessage prm = PlayerReadyMessage();

        PlayerReadyMessageHandler* prmh = new PlayerReadyMessageHandler(new DefaultHandler());
        Response *response = prmh->handleMessage(&prm, serverThread);

        PlayerReadyResponse* prr = dynamic_cast<PlayerReadyResponse*>(response);
        REQUIRE(prr != nullptr);

        REQUIRE(prr->getSuccess() == false);
        REQUIRE(prr->getErrorMessage() == "Niste povezani u partiju!");
    }

    SECTION("PlayerReadyHandlerTest : Igrac je vec spreman")
    {
        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        serverThread->gameManager = Server::getInstance()->generateNewGame();
        serverThread->color = serverThread->gameManager->joinGame(serverThread);

        PlayerReadyMessage prm = PlayerReadyMessage();

        PlayerReadyMessageHandler* prmh = new PlayerReadyMessageHandler(new DefaultHandler());
        Response *response = prmh->handleMessage(&prm, serverThread);

        PlayerReadyResponse* prr = dynamic_cast<PlayerReadyResponse*>(response);
        REQUIRE(prr != nullptr);

        REQUIRE(prr->getSuccess() == true);
        REQUIRE(prr->getColor() == serverThread->color);
        REQUIRE(prr->getErrorMessage() == "");

        PlayerReadyMessage prm1 = PlayerReadyMessage();

        Response *response1 = prmh->handleMessage(&prm1, serverThread);

        PlayerReadyResponse* prr1 = dynamic_cast<PlayerReadyResponse*>(response1);
        REQUIRE(prr1 != nullptr);

        REQUIRE(prr1->getSuccess() == false);
        REQUIRE(prr1->getColor() == serverThread->color);
        REQUIRE(prr1->getErrorMessage() == "Igrac je vec spreman");

    }

}
