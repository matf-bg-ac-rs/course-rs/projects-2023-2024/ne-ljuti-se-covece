# :game_die: Ne ljuti se covece

## :pencil: Opis igre

Igra u stilu svima poznate igre ne ljuti se covece, obogacena novim funkcionalnostima i nepredvidljivim preokretima. Prilagodjena je igranju vise igraca.

Partija uvek ima 4 aktivna ucesnika. Medjutim igrac koji kreira igru ima mogucnost da izabere da li ce i u kom broju u partiju ukljuciti i robotske igrace.

Osnovna pravila i cilj igre je isti kao i u standardnoj verziji. Igrac zeli da sve 4 svoje figurice dovede na sigurno (u kucicu) i da sabotira protivnicke
igrace da svoje figurice dovedu u kucicu pre njega. Omogucen je potez "jedenje" u kom je igracu dozvoljeno, ako je potez pomeranja validan za datog igraca,
da okupira i polje na kom je vec postavljena protivnicka figurica. U tom slcaju figurica koja je prva bila na polju biva vracena na pocetak, a nova figurica 
preuzima njeno mesto na tabli. Tada smatramo da je jedan igrac "pojeo" figuricu drugog igraca.

Osim standardnih pravila koja veze za igranje standardne igre Ne ljuti se covece na raspolaganju su i nove pomocne magije.
Magije imaju za cilj da partiju naprave zanimljivijom, neizvesnijom i dinamicnijom. 

Dostupne magije su :
    - Stit : Omogucava igracu da zastiti svoje figurice "jedenja" od strane protivnika u toku jednog kruga 
    - Dupla kockica : Omogucava igracu da nakon bacanja kockice, duplira broj polja za pomeranje u datom potezu
    - +1 : Omogucava igracu da nakon bacanja kockice, broj polja za pomeranje u datom potezu poveca za 1
    - -1 : Omogucava igracu da nakon bacanja kockice, broj polja za pomeranje u datom potezu smanji za 1
    - Remote Dice : Omogucava igracu da pre bacanja kockice, odabere broj na kockici koji mu u datom potezu odgovara

Pobednik je igrac koji prvi smesti svoje 4 figurice u kucicu.

---

## :movie_camera: Demo snimak projekta
- link : [NeLjutiSeCovece](https://youtu.be/Djs76lsDaSQ)

---
## :computer: Programski jezik

- [![c_plus_plus](https://img.shields.io/badge/Language-C%2B%2B-red)](https://www.cplusplus.com/)<br>
- [![qt5](https://img.shields.io/badge/Framework-Qt5-blue)](https://doc.qt.io/qt-6/)  <br><br>

## :notebook: Okruzenje i biblioteke

- [![qtCreator](https://img.shields.io/badge/IDE-Qt_Creator-olivia)](https://www.qt.io/download) <br><br>
- Catch2

## :hammer: Preuzimanje i pokretanje

- 1. U terminalu se pozicionirati u zeljeni direktorijum
- 2. Klonirati repozitorijum komandom: `$ git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2023-2024/ne-ljuti-se-covece.git`
- 3. Otvoriti okruzenje *Qt Creator* i u njemu otvoriti CMakeList.txt fajl
- 4. Pritisnuti dugme Run u donjem levom uglu ekrana za server, a zatim za game

---

Članovi:

 - <a href="https://gitlab.com/ankastankovic98">Anka Stankovic 404/2021</a>
 - <a href="https://gitlab.com/andrija_rangl">Andrija Rangl 168/2018</a>
 - <a href="https://gitlab.com/Milica_0190">Milica Radivojevic 190/2019</a>
 - <a href="https://gitlab.com/vladimir_0102">Vladimir Kastratovic 31/2019</a>
 - <a href="https://gitlab.com/minjastankov">Minja Stankov 391/2021</a>
 - <a href="https://gitlab.com/EmilijaNed">Emilija Nedeljkovic 43/2020</a>
