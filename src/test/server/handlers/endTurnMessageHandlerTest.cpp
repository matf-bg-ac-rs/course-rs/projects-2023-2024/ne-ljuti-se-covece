#include "../../catch.hpp"
#include "../../../common/message.h"
#include "../../../server/participants.h"
#include "../../../server/server.h"
#include "../../../server/handlers.h"
#include "../../../server/managers.h"

TEST_CASE("EndTurnMessageHandlerTest", "[EndTurnMessageHandlerTest]")
{
    SECTION("EndTurnMessageHandlerTest : Uspesno zavrsen potez")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();

        ServerThreadParticipant *prvi = new ServerThreadParticipant(1, nullptr);
        prvi->gameManager = gameManager;
        prvi->color = prvi->gameManager->joinGame(prvi);
        prvi->gameManager->playerReadyList[0] = true;

        ServerThreadParticipant *drugi = new ServerThreadParticipant(2, nullptr);
        drugi->gameManager = gameManager;
        drugi->color = drugi->gameManager->joinGame(drugi);
        drugi->gameManager->playerReadyList[1] = true;

        ServerThreadParticipant *treci = new ServerThreadParticipant(3, nullptr);
        treci->gameManager = gameManager;
        treci->color = treci->gameManager->joinGame(treci);
        treci->gameManager->playerReadyList[2] = true;

        ServerThreadParticipant *cetvrti = new ServerThreadParticipant(4, nullptr);
        cetvrti->gameManager = gameManager;
        cetvrti->color = cetvrti->gameManager->joinGame(cetvrti);
        cetvrti->gameManager->playerReadyList[3] = true;

        gameManager->getTurnContext()->useOneRoll();
        gameManager->getTurnContext()->setLastRolledValue(5);
        gameManager->getTurnContext()->useOneMove();
        gameManager->getTurnContext()->setCurrentPlayerColor(prvi->color);

        EndTurnMessage etm = EndTurnMessage();

        EndTurnMessageHandler* etmh = new EndTurnMessageHandler(new DefaultHandler());

        Response *response = etmh->handleMessage(&etm, prvi);
        EndTurnResponse* etr = dynamic_cast<EndTurnResponse*>(response);

        REQUIRE(etr != nullptr);

        REQUIRE(etr->getSuccess() == true);
        REQUIRE(etr->shouldBroadcast() == true);
        REQUIRE(etr->getColor() == drugi->color);
        REQUIRE(etr->getErrorMessage() == "");
    }

    SECTION("EndTurnMessageHandlerTest : Niste na potezu, molimo sacekajte trenutnog igraca!")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();

        ServerThreadParticipant *prvi = new ServerThreadParticipant(1, nullptr);
        prvi->gameManager = gameManager;
        prvi->color = prvi->gameManager->joinGame(prvi);
        prvi->gameManager->playerReadyList[0] = true;

        ServerThreadParticipant *drugi = new ServerThreadParticipant(2, nullptr);
        drugi->gameManager = gameManager;
        drugi->color = drugi->gameManager->joinGame(drugi);
        drugi->gameManager->playerReadyList[1] = true;

        ServerThreadParticipant *treci = new ServerThreadParticipant(3, nullptr);
        treci->gameManager = gameManager;
        treci->color = treci->gameManager->joinGame(treci);
        treci->gameManager->playerReadyList[2] = true;

        ServerThreadParticipant *cetvrti = new ServerThreadParticipant(4, nullptr);
        cetvrti->gameManager = gameManager;
        cetvrti->color = cetvrti->gameManager->joinGame(cetvrti);
        cetvrti->gameManager->playerReadyList[3] = true;

        gameManager->getTurnContext()->setCurrentPlayerColor(treci->color);

        EndTurnMessage etm = EndTurnMessage();

        EndTurnMessageHandler* etmh = new EndTurnMessageHandler(new DefaultHandler());

        Response *response = etmh->handleMessage(&etm, prvi);
        EndTurnResponse* etr = dynamic_cast<EndTurnResponse*>(response);

        REQUIRE(etr != nullptr);

        REQUIRE(etr->getSuccess() == false);
        REQUIRE(etr->getColor() == prvi->color);
        REQUIRE(etr->shouldBroadcast() == false);
        REQUIRE(etr->getErrorMessage() == "Niste na potezu, molimo sacekajte trenutnog igraca!");
    }

    SECTION("EndTurnMessageHandlerTest : Morate baciti kockicu!")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();

        ServerThreadParticipant *prvi = new ServerThreadParticipant(1, nullptr);
        prvi->gameManager = gameManager;
        prvi->color = prvi->gameManager->joinGame(prvi);
        prvi->gameManager->playerReadyList[0] = true;

        ServerThreadParticipant *drugi = new ServerThreadParticipant(2, nullptr);
        drugi->gameManager = gameManager;
        drugi->color = drugi->gameManager->joinGame(drugi);
        drugi->gameManager->playerReadyList[1] = true;

        ServerThreadParticipant *treci = new ServerThreadParticipant(3, nullptr);
        treci->gameManager = gameManager;
        treci->color = treci->gameManager->joinGame(treci);
        treci->gameManager->playerReadyList[2] = true;

        ServerThreadParticipant *cetvrti = new ServerThreadParticipant(4, nullptr);
        cetvrti->gameManager = gameManager;
        cetvrti->color = cetvrti->gameManager->joinGame(cetvrti);
        cetvrti->gameManager->playerReadyList[3] = true;

        gameManager->getTurnContext()->setCurrentPlayerColor(prvi->color);

        EndTurnMessage etm = EndTurnMessage();

        EndTurnMessageHandler* etmh = new EndTurnMessageHandler(new DefaultHandler());

        Response *response = etmh->handleMessage(&etm, prvi);
        EndTurnResponse* etr = dynamic_cast<EndTurnResponse*>(response);

        REQUIRE(etr != nullptr);

        REQUIRE(etr->getSuccess() == false);
        REQUIRE(etr->getColor() == prvi->color);
        REQUIRE(etr->shouldBroadcast() == false);
        REQUIRE(etr->getErrorMessage() == "Morate baciti kockicu!");
    }

    SECTION("EndTurnMessageHandlerTest : Imate slobodan potez, morate pomeriti figuricu!")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();

        ServerThreadParticipant *prvi = new ServerThreadParticipant(1, nullptr);
        prvi->gameManager = gameManager;
        prvi->color = prvi->gameManager->joinGame(prvi);
        prvi->gameManager->playerReadyList[0] = true;

        ServerThreadParticipant *drugi = new ServerThreadParticipant(2, nullptr);
        drugi->gameManager = gameManager;
        drugi->color = drugi->gameManager->joinGame(drugi);
        drugi->gameManager->playerReadyList[1] = true;

        ServerThreadParticipant *treci = new ServerThreadParticipant(3, nullptr);
        treci->gameManager = gameManager;
        treci->color = treci->gameManager->joinGame(treci);
        treci->gameManager->playerReadyList[2] = true;

        ServerThreadParticipant *cetvrti = new ServerThreadParticipant(4, nullptr);
        cetvrti->gameManager = gameManager;
        cetvrti->color = cetvrti->gameManager->joinGame(cetvrti);
        cetvrti->gameManager->playerReadyList[3] = true;

        gameManager->getTurnContext()->setCurrentPlayerColor(prvi->color);
        gameManager->getTurnContext()->useOneRoll();
        gameManager->getTurnContext()->setLastRolledValue(6);

        EndTurnMessage etm = EndTurnMessage();

        EndTurnMessageHandler* etmh = new EndTurnMessageHandler(new DefaultHandler());

        Response *response = etmh->handleMessage(&etm, prvi);
        EndTurnResponse* etr = dynamic_cast<EndTurnResponse*>(response);

        REQUIRE(etr != nullptr);

        REQUIRE(etr->getSuccess() == false);
        REQUIRE(etr->getColor() == prvi->color);
        REQUIRE(etr->shouldBroadcast() == false);
        REQUIRE(etr->getErrorMessage() == "Imate slobodan potez, morate pomeriti figuricu!");
    }

}
