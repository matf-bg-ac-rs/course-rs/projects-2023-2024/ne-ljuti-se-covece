#ifndef HANDLERS_H
#define HANDLERS_H

#include <QObject>
#include "../common/message.h"
#include "participants.h"

class GameManager;

class ChainElement : public QObject
{
public:
    ChainElement* nextElement;

    explicit ChainElement(ChainElement* nextElement);

    virtual Response* handleMessage(Message* message, BaseParticipant *participant) = 0;
};

class DefaultHandler : public ChainElement
{
public:

    explicit DefaultHandler(ChainElement* nextElement = nullptr)
        : ChainElement {nextElement} {};

    Response* handleMessage(Message* message, BaseParticipant *participant)override;
};

class CreateGameMessageHandler : public ChainElement
{
public:

    explicit CreateGameMessageHandler(ChainElement* nextElement)
        : ChainElement {nextElement} {};

    Response* handleMessage(Message* message, BaseParticipant *participant) override;
};

class JoinGameMessageHandler : public ChainElement
{
public:

    explicit JoinGameMessageHandler(ChainElement* nextElement)
        : ChainElement {nextElement} {};

    Response* handleMessage(Message* message, BaseParticipant *participant) override;
};

class PlayerReadyMessageHandler : public ChainElement
{
public:

    explicit PlayerReadyMessageHandler(ChainElement* nextElement)
        : ChainElement {nextElement} {};

    Response* handleMessage(Message* message, BaseParticipant *participant) override;
};

class StartGameMessageHandler : public ChainElement
{
public:

    explicit StartGameMessageHandler(ChainElement* nextElement)
        : ChainElement {nextElement} {};

    Response* handleMessage(Message* message, BaseParticipant *participant) override;
};


class EndTurnMessageHandler : public ChainElement
{
public:

    explicit EndTurnMessageHandler(ChainElement* nextElement)
        : ChainElement {nextElement} {};

    Response* handleMessage(Message* message, BaseParticipant *participant) override;
};

class RollDiceMessageHandler : public ChainElement
{
public:

    explicit RollDiceMessageHandler(ChainElement* nextElement)
        : ChainElement {nextElement} {};

    Response* handleMessage(Message* message, BaseParticipant *participant) override;
};

class MovePawnMessageHandler : public ChainElement
{
public:

    explicit MovePawnMessageHandler(ChainElement* nextElement)
        : ChainElement {nextElement} {};

    Response* handleMessage(Message* message, BaseParticipant *participant) override;
};

class ActivateMagicMessageHandler : public ChainElement
{
public:

    explicit ActivateMagicMessageHandler(ChainElement* nextElement)
        : ChainElement {nextElement} {};

    Response* handleMessage(Message* message, BaseParticipant *participant) override;
};

#endif // HANDLERS_H
