#include "../../catch.hpp"
#include "../../../common/message.h"
#include "../../../server/participants.h"
#include "../../../server/server.h"
#include "../../../server/handlers.h"
#include "../../../server/managers.h"

TEST_CASE("ActivateMagicMessageHandlerTest", "[ActivateMagicMessageHandler]")
{

    SECTION("ActivateMagicMessageHandlerTest: Valid ActivateMagicMessage Handling")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();
        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        serverThread->gameManager = gameManager;
        serverThread->color = Color::Blue;

        ActivateMagicMessageHandler *handler = new ActivateMagicMessageHandler(new DefaultHandler());

        ActivateMagicMessage validMessage = ActivateMagicMessage("shield", Color::Blue);
        Response* response = handler->handleMessage(&validMessage, serverThread);
        REQUIRE(response != nullptr);

        ActivateMagicResponse* moveResponse = dynamic_cast<ActivateMagicResponse*>(response);
        REQUIRE(moveResponse->getColor() == Color::Blue);
        REQUIRE(moveResponse->getErrorMessage() == "");
    }

    SECTION("ActivateMagicMessageHandlerTest: Invalid ActivateMagicMessage Handling, wrong color")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();
        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        serverThread->gameManager = gameManager;
        serverThread->color = Color::Blue;

        gameManager->getTurnContext()->setCurrentPlayerColor(Color::Red);

        ActivateMagicMessageHandler *handler = new ActivateMagicMessageHandler(new DefaultHandler());

        ActivateMagicMessage validMessage = ActivateMagicMessage("shield", Color::Red);
        Response* response = handler->handleMessage(&validMessage, serverThread);
        REQUIRE(response != nullptr);

        ActivateMagicResponse* moveResponse = dynamic_cast<ActivateMagicResponse*>(response);
        REQUIRE(moveResponse->getErrorMessage() == "Ne mozete koristiti magiju, jer nije vas potez!");
    }
    SECTION("ActivateMagicMessageHandlerTest: Invalid ActivateMagicMessage Handling, no magic")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();
        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        serverThread->gameManager = gameManager;
        serverThread->color = Color::Blue;

        gameManager->useMagic("shield");
        gameManager->useMagic("shield");
        gameManager->useMagic("shield");
        gameManager->useMagic("shield");

        ActivateMagicMessageHandler *handler = new ActivateMagicMessageHandler(new DefaultHandler());

        ActivateMagicMessage validMessage = ActivateMagicMessage("shield", Color::Red);
        Response* response = handler->handleMessage(&validMessage, serverThread);
        REQUIRE(response != nullptr);

        ActivateMagicResponse* moveResponse = dynamic_cast<ActivateMagicResponse*>(response);
        REQUIRE(moveResponse->getErrorMessage() == "Nemate ovu magiju na raspolaganju!");
    }

    SECTION("ActivateMagicMessageHandlerTest: Invalid ActivateMagicMessage Handling, before roll dice")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();
        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        serverThread->gameManager = gameManager;
        serverThread->color = Color::Blue;

        gameManager->getTurnContext()->useOneRoll();

        ActivateMagicMessageHandler *handler = new ActivateMagicMessageHandler(new DefaultHandler());

        ActivateMagicMessage validMessage = ActivateMagicMessage("remote_dice", Color::Blue);
        Response* response = handler->handleMessage(&validMessage, serverThread);
        REQUIRE(response != nullptr);

        ActivateMagicResponse* moveResponse = dynamic_cast<ActivateMagicResponse*>(response);
        REQUIRE(moveResponse->getErrorMessage() == "Ne mozete iskoristi ovu magiju nakon bacanja kockice!");
    }
    SECTION("ActivateMagicMessageHandlerTest: Invalid ActivateMagicMessage Handling, after roll dice")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();
        ServerThreadParticipant *serverThread = new ServerThreadParticipant(1, nullptr);
        serverThread->gameManager = gameManager;
        serverThread->color = Color::Blue;

        gameManager->getTurnContext()->gainOneRoll();

        ActivateMagicMessageHandler *handler = new ActivateMagicMessageHandler(new DefaultHandler());

        ActivateMagicMessage validMessage = ActivateMagicMessage("minus_1", Color::Blue);
        Response* response = handler->handleMessage(&validMessage, serverThread);
        REQUIRE(response != nullptr);

        ActivateMagicResponse* moveResponse = dynamic_cast<ActivateMagicResponse*>(response);
        REQUIRE(moveResponse->getErrorMessage() == "Ne mozete iskoristi ovu magiju pre bacanje kockice!");
    }

}
