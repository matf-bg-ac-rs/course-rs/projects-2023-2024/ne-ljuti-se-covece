#include "clientmanagers.h"
#include "../../common/messagefactory.h"

ClientMessageManager::ClientMessageManager(QObject *parent)
    : QObject{parent}
{
    this->chainElement =
        new ActivateMagicResponseHandler(
            new MovePawnResponseHandler(
                new EndGameResponseHandler(
                    new RollDiceResponseHandler(
                        new EndTurnResponseHandler(
                            new StartGameResponseHandler(
                                new PlayerReadyResponseHandler(
                                    new JoinGameResponseHandler(
                                            new DefaultResponseHandler()))))))));
}

void ClientMessageManager::handleMessage(const QByteArray &rawMessage){
    Response* response = (Response*)MessageFactory::createMessage(rawMessage);
    qDebug() << "Od servera je stiglo: "<< response->prepareMessage();
    this->chainElement->handleResponse(response);

    delete response;
    return;
}

