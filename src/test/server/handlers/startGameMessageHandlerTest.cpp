#include "../../catch.hpp"
#include "../../../common/message.h"
#include "../../../server/participants.h"
#include "../../../server/server.h"
#include "../../../server/handlers.h"
#include "../../../server/managers.h"

TEST_CASE("StartGameMessageHandlerTest", "[StartGameMessageHandlerTest]")
{
    SECTION("StartGameMessageHandlerTest : Uspesno startovanje partije.")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();

        ServerThreadParticipant *prvi = new ServerThreadParticipant(1, nullptr);
        prvi->gameManager = gameManager;
        prvi->color = prvi->gameManager->joinGame(prvi);
        prvi->gameManager->playerReadyList[0] = true;

        ServerThreadParticipant *drugi = new ServerThreadParticipant(2, nullptr);
        drugi->gameManager = gameManager;
        drugi->color = drugi->gameManager->joinGame(drugi);
        drugi->gameManager->playerReadyList[1] = true;

        ServerThreadParticipant *treci = new ServerThreadParticipant(3, nullptr);
        treci->gameManager = gameManager;
        treci->color = treci->gameManager->joinGame(treci);
        treci->gameManager->playerReadyList[2] = true;

        ServerThreadParticipant *cetvrti = new ServerThreadParticipant(4, nullptr);
        cetvrti->gameManager = gameManager;
        cetvrti->color = cetvrti->gameManager->joinGame(cetvrti);
        cetvrti->gameManager->playerReadyList[3] = true;


        StartGameMessage sgm = StartGameMessage();

        StartGameMessageHandler* sgmh = new StartGameMessageHandler(new DefaultHandler());
        Response *response = sgmh->handleMessage(&sgm, prvi);

        StartGameResponse* sgr = dynamic_cast<StartGameResponse*>(response);
        REQUIRE(sgr != nullptr);

        REQUIRE(sgr->getSuccess() == true);
        REQUIRE(sgr->getErrorMessage() == "");
        REQUIRE(sgr->getColor() == prvi->color);
    }

    SECTION("StartGameMessageHandlerTest : Igra je vec startovana, ne moze se ponovo startovati!")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();

        ServerThreadParticipant *prvi = new ServerThreadParticipant(1, nullptr);
        prvi->gameManager = gameManager;
        prvi->color = prvi->gameManager->joinGame(prvi);
        prvi->gameManager->playerReadyList[0] = true;

        ServerThreadParticipant *drugi = new ServerThreadParticipant(2, nullptr);
        drugi->gameManager = gameManager;
        drugi->color = drugi->gameManager->joinGame(drugi);
        drugi->gameManager->playerReadyList[1] = true;

        ServerThreadParticipant *treci = new ServerThreadParticipant(3, nullptr);
        treci->gameManager = gameManager;
        treci->color = treci->gameManager->joinGame(treci);
        treci->gameManager->playerReadyList[2] = true;

        ServerThreadParticipant *cetvrti = new ServerThreadParticipant(4, nullptr);
        cetvrti->gameManager = gameManager;
        cetvrti->color = cetvrti->gameManager->joinGame(cetvrti);
        cetvrti->gameManager->playerReadyList[3] = true;


        StartGameMessage sgm = StartGameMessage();

        StartGameMessageHandler* sgmh = new StartGameMessageHandler(new DefaultHandler());
        Response *response = sgmh->handleMessage(&sgm, prvi);

        StartGameResponse* sgr = dynamic_cast<StartGameResponse*>(response);
        REQUIRE(sgr != nullptr);

        REQUIRE(sgr->getSuccess() == true);
        REQUIRE(sgr->getErrorMessage() == "");
        REQUIRE(sgr->getColor() == prvi->color);

        StartGameMessage sgm1 = StartGameMessage();

        Response *response1 = sgmh->handleMessage(&sgm1, prvi);

        StartGameResponse* sgr1 = dynamic_cast<StartGameResponse*>(response1);
        REQUIRE(sgr1 != nullptr);

        REQUIRE(sgr1->getSuccess() == false);
        REQUIRE(sgr1->getColor() == prvi->color);
        REQUIRE(sgr1->getErrorMessage() == "Igra je vec startovana, ne moze se ponovo startovati!");
    }

    SECTION("StartGameMessageHandlerTest : Nisu ispunjeni uslovi za startovanje igre, igraci nisu Ready")
    {
        GameManager* gameManager = Server::getInstance()->generateNewGame();

        ServerThreadParticipant *prvi = new ServerThreadParticipant(1, nullptr);
        prvi->gameManager = gameManager;
        prvi->color = prvi->gameManager->joinGame(prvi);

        ServerThreadParticipant *drugi = new ServerThreadParticipant(2, nullptr);
        drugi->gameManager = gameManager;
        drugi->color = drugi->gameManager->joinGame(drugi);

        ServerThreadParticipant *treci = new ServerThreadParticipant(3, nullptr);
        treci->gameManager = gameManager;
        treci->color = treci->gameManager->joinGame(treci);

        ServerThreadParticipant *cetvrti = new ServerThreadParticipant(4, nullptr);
        cetvrti->gameManager = gameManager;
        cetvrti->color = cetvrti->gameManager->joinGame(cetvrti);
        cetvrti->gameManager->playerReadyList[3] = true;

        StartGameMessage sgm = StartGameMessage();

        StartGameMessageHandler* sgmh = new StartGameMessageHandler(new DefaultHandler());
        Response *response = sgmh->handleMessage(&sgm, prvi);

        StartGameResponse* sgr = dynamic_cast<StartGameResponse*>(response);
        REQUIRE(sgr != nullptr);

        REQUIRE(sgr->getSuccess() == false);
        REQUIRE(sgr->getErrorMessage() == "Nisu ispunjeni uslovi za startovanje igre");
        REQUIRE(sgr->getColor() == prvi->color);
    }
}
