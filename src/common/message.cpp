#include "message.h"
#include <QJsonDocument>

QByteArray Message::prepareMessage()
{
    QJsonObject jsonObj = this->toJson();
    QJsonDocument jsonDoc(jsonObj);
    QString jsonString = jsonDoc.toJson(QJsonDocument::Compact);
    return jsonString.toUtf8();
}

QString Response::getErrorMessage() const
{
    return errorMessage;
}

bool Response::getSuccess() const
{
    return success;
}

bool Response::shouldBroadcast() const
{
    return broadcast;
}

#pragma region CreateGameMessage
QString CreateGameMessage::TYPE = "CreateGameMessage";

CreateGameMessage::CreateGameMessage(int numberOfPlayers)
{
    this->numberOfPlayers = numberOfPlayers;
};

CreateGameMessage::CreateGameMessage(const QJsonObject &json)
{
    this->numberOfPlayers = json["numberOfPlayers"].toInteger();
}

int CreateGameMessage::getNumberOfPlayers() const
{
    return numberOfPlayers;
};

QJsonObject CreateGameMessage::toJson()
{
    QJsonObject json;
    json["type"] = CreateGameMessage::TYPE;
    json["numberOfPlayers"] = this->numberOfPlayers;
    return json;
}
#pragma endregion

#pragma region CreateGameResponse
QString CreateGameResponse::TYPE = "CreateGameResponse";

CreateGameResponse::CreateGameResponse(QString gameCode, Color color)
{
    this->gameCode = gameCode;
    this->color = color;
    this->broadcast = false;
    this->success = true;
}

CreateGameResponse::CreateGameResponse(QString errorMessage)
{
    this->errorMessage = errorMessage;
    this->broadcast = false;
    this->success = false;
}

CreateGameResponse::CreateGameResponse(const QJsonObject &json)
{
    this->gameCode = json["gameCode"].toString();
    this->color = static_cast<Color>(json["color"].toInteger());
    this->errorMessage = json["errorMessage"].toString();
    this->success = json["success"].toBool();
}

Color CreateGameResponse::getColor() const
{
    return color;
}

QString CreateGameResponse::getGameCode() const
{
    return gameCode;
};

QJsonObject CreateGameResponse::toJson()
{
    QJsonObject json;
    json["type"] = CreateGameResponse::TYPE;
    json["gameCode"] = this->gameCode;
    json["color"] = this->color;
    json["errorMessage"] = this->errorMessage;
    json["success"] = this->success;
    return json;
}
#pragma endregion

#pragma region JoinGameMessage
QString JoinGameMessage::TYPE = "JoinGameMessage";

JoinGameMessage::JoinGameMessage(QString gameCode)
{
    this->gameCode = gameCode;
};

JoinGameMessage::JoinGameMessage(const QJsonObject &json)
{
    this->gameCode = json["gameCode"].toString();
}

QString JoinGameMessage::getGameCode() const
{
    return gameCode;
};

QJsonObject JoinGameMessage::toJson()
{
    QJsonObject json;
    json["type"] = JoinGameMessage::TYPE;
    json["gameCode"] = this->gameCode;
    return json;
}
#pragma endregion

#pragma region JoinGameResponse
QString JoinGameResponse::TYPE = "JoinGameResponse";

JoinGameResponse::JoinGameResponse(bool joined, Color color, QString errorMessage)
{
    this->joined = joined;
    this->errorMessage = errorMessage;
    this->broadcast = joined;
    this->color = color;
    this->success = joined;
}

JoinGameResponse::JoinGameResponse(const QJsonObject &json)
{
    this->joined = json["joined"].toBool();
    this->errorMessage = json["errorMessage"].toString();
    this->color = static_cast<Color>(json["color"].toInteger());
    this->success = json["success"].toBool();
}

bool JoinGameResponse::getJoined() const
{
    return joined;
}

Color JoinGameResponse::getColor() const
{
    return color;
};

QJsonObject JoinGameResponse::toJson()
{
    QJsonObject json;
    json["type"] = JoinGameResponse::TYPE;
    json["joined"] = this->joined;
    json["errorMessage"] = this->errorMessage;
    json["color"] = this->color;
    json["success"] = this->success;
    return json;
}
#pragma endregion


#pragma region PlayerReadyMessage
QString PlayerReadyMessage::TYPE = "PlayerReadyMessage";

PlayerReadyMessage::PlayerReadyMessage()
{
};

PlayerReadyMessage::PlayerReadyMessage(const QJsonObject &json)
{
};

QJsonObject PlayerReadyMessage::toJson()
{
    QJsonObject json;
    json["type"] = PlayerReadyMessage::TYPE;
    return json;
}
#pragma endregion


#pragma region PlayerReadyResponse
QString PlayerReadyResponse::TYPE = "PlayerReadyResponse";

PlayerReadyResponse::PlayerReadyResponse(Color color, bool success, QString errorMessage)
{
    this->color = color;
    this->success = success;
    this->broadcast = success;
    this->errorMessage = errorMessage;
}

PlayerReadyResponse::PlayerReadyResponse(const QJsonObject &json)
{
    this->broadcast = true;
    this->color = static_cast<Color>(json["color"].toInteger());
    this->errorMessage = json["errorMessage"].toString();
    this->success = json["success"].toBool();
}

Color PlayerReadyResponse::getColor() const
{
    return color;
};

QJsonObject PlayerReadyResponse::toJson()
{
    QJsonObject json;
    json["type"] = PlayerReadyResponse::TYPE;
    json["color"] = this->color;
    json["errorMessage"] = this->errorMessage;
    json["success"] = this->success;
    return json;
}
#pragma endregion

#pragma region StartGameMessage
QString StartGameMessage::TYPE = "StartGameMessage";

StartGameMessage::StartGameMessage()
{
};

StartGameMessage::StartGameMessage(const QJsonObject &json)
{
};

QJsonObject StartGameMessage::toJson()
{
    QJsonObject json;
    json["type"] = StartGameMessage::TYPE;
    return json;
}
#pragma endregion


#pragma region StartGameResponse
QString StartGameResponse::TYPE = "StartGameResponse";

StartGameResponse::StartGameResponse(Color color, bool success, QString errorMessage)
{
    this->color = color;
    this->success = success;
    this->broadcast = success;
    this->errorMessage = errorMessage;
}

StartGameResponse::StartGameResponse(const QJsonObject &json)
{
    this->broadcast = true;
    this->color = static_cast<Color>(json["color"].toInteger());
    this->errorMessage = json["errorMessage"].toString();
    this->success = json["success"].toBool();
}

Color StartGameResponse::getColor() const
{
    return color;
}

QJsonObject StartGameResponse::toJson()
{
    QJsonObject json;
    json["type"] = StartGameResponse::TYPE;
    json["color"] = this->color;
    json["errorMessage"] = this->errorMessage;
    json["success"] = this->success;
    return json;
}
#pragma endregion

#pragma region EndTurnMessage
QString EndTurnMessage::TYPE = "EndTurnMessage";

EndTurnMessage::EndTurnMessage()
{
};

EndTurnMessage::EndTurnMessage(const QJsonObject &json)
{
};

QJsonObject EndTurnMessage::toJson()
{
    QJsonObject json;
    json["type"] = EndTurnMessage::TYPE;
    return json;
}
#pragma endregion


#pragma region EndTurnResponse
QString EndTurnResponse::TYPE = "EndTurnResponse";

EndTurnResponse::EndTurnResponse(Color color, bool success, QString errorMessage)
{
    this->color = color;
    this->errorMessage = errorMessage;
    this->broadcast = success;
    this->success = success;
}

EndTurnResponse::EndTurnResponse(const QJsonObject &json)
{
    this->broadcast = true;
    this->color = static_cast<Color>(json["color"].toInteger());
    this->errorMessage = json["errorMessage"].toString();
    this->success = json["success"].toBool();
}

QJsonObject EndTurnResponse::toJson()
{
    QJsonObject json;
    json["type"] = EndTurnResponse::TYPE;
    json["color"] = this->color;
    json["errorMessage"] = this->errorMessage;
    json["success"] = this->success;
    return json;
}

Color EndTurnResponse::getColor() const
{
    return color;
};
#pragma endregion

#pragma region RollDiceMessage
QString RollDiceMessage::TYPE = "RollDiceMessage";

RollDiceMessage::RollDiceMessage()
{

}

RollDiceMessage::RollDiceMessage(const QJsonObject &json)
{

}

QJsonObject RollDiceMessage::toJson()
{
    QJsonObject json;
    json["type"] = RollDiceMessage::TYPE;
    return json;
}

#pragma endregion

#pragma region RollDiceResponse

QString RollDiceResponse::TYPE = "RollDiceResponse";

RollDiceResponse::RollDiceResponse(int number, Color color)
{
    this->number = number;
    this->broadcast = true;
    this->success = true;
    this->color = color;
}

RollDiceResponse::RollDiceResponse(Color color, QString errorMessage)
{
    this->color = color;
    this->errorMessage = errorMessage;
    this->success = false;
    this->number = 0;
    this->broadcast = false;
}

RollDiceResponse::RollDiceResponse(const QJsonObject &json)
{
    this->number = json["number"].toInt();
    this->color = static_cast<Color>(json["color"].toInteger());
    this->errorMessage = json["errorMessage"].toString();
    this->success = json["success"].toBool();
}

QJsonObject RollDiceResponse::toJson()
{
    QJsonObject json;
    json["type"] = RollDiceResponse::TYPE;
    json["number"] = this->number;    
    json["color"] = this->color;
    json["errorMessage"] = this->errorMessage;
    json["success"] = this->success;
    return json;
}

int RollDiceResponse::getNumber() const
{
    return number;
}

Color RollDiceResponse::getColor() const
{
    return color;
}

#pragma endregion

#pragma region EndGameResponse

QString EndGameResponse::TYPE = "EndGameResponse";

EndGameResponse::EndGameResponse(Color color)
{
    this->color = color;
    this->broadcast = true;
    this->success = true;
}
EndGameResponse::EndGameResponse(Color color, QString errorMessage)
{
    this->color = color;
    this->errorMessage = errorMessage;
    this->success = false;
    this->broadcast = false;
}
EndGameResponse::EndGameResponse(const QJsonObject &json)
{
    this->color = static_cast<Color>(json["color"].toInteger());
    this->success = json["success"].toBool();
    this->errorMessage = json["errorMessage"].toString();
}

QJsonObject EndGameResponse::toJson()
{
    QJsonObject json;
    json["type"] = EndGameResponse::TYPE;
    json["color"] = this->color;
    json["success"] = this->success;
    json["errorMessage"] = this->errorMessage;
    return json;
}

Color EndGameResponse::getColor() const
{
    return color;
}

#pragma endregion

#pragma region MovePawnMessage

QString MovePawnMessage::TYPE = "MovePawnMessage";

MovePawnMessage::MovePawnMessage(QString field, Color pawnColor)
{
    this->field = field;
    this->pawnColor = pawnColor;
}

MovePawnMessage::MovePawnMessage(const QJsonObject &json)
{
    this->field = json["field"].toString();
    this->pawnColor = static_cast<Color>(json["pawnColor"].toInteger());
}

QJsonObject MovePawnMessage::toJson()
{
    QJsonObject json;
    json["type"] = MovePawnMessage::TYPE;
    json["field"] = this->field;
    json["pawnColor"] = this->pawnColor;
    return json;
}

#pragma endregion

#pragma region MovePawnResponse

QString MovePawnResponse::TYPE = "MovePawnResponse";

MovePawnResponse::MovePawnResponse(QString field, Color color, QString newField, QString eatenPawnNewField, Color eatenColor)
{
    this->newField = newField;
    this->eatenPawnNewField = eatenPawnNewField;
    this->color = color;
    this->eatenColor = eatenColor;
    this->oldField = field;
    this->broadcast = true;
    this->success = true;
}

MovePawnResponse::MovePawnResponse(const QJsonObject &json)
{
    this->color = static_cast<Color>(json["color"].toInteger());
    this->eatenColor = static_cast<Color>(json["eatenColor"].toInteger());
    this->newField = json["newField"].toString();
    this->eatenPawnNewField = json["eatenPawnNewField"].toString();
    this->oldField = json["oldField"].toString();
    this->success = json["success"].toBool();
    this->errorMessage = json["errorMessage"].toString();
}

MovePawnResponse::MovePawnResponse(Color color, QString errorMessage)
{
    this->color = color;
    this->errorMessage = errorMessage;
    this->success = false;
    this->broadcast = false;
}

QJsonObject MovePawnResponse::toJson()
{
    QJsonObject json;
    json["type"] = MovePawnResponse::TYPE;
    json["color"] = this->color;
    json["eatenColor"] = this->eatenColor;
    json["newField"] = this->newField;
    json["eatenPawnNewField"] = this->eatenPawnNewField;
    json["oldField"] = this->oldField;
    json["errorMessage"] = this->errorMessage;
    json["success"] = this->success;
    return json;
}

Color MovePawnResponse::getColor() const
{
    return color;
}

QString MovePawnResponse::getNewField() const
{
    return newField;
}

QString MovePawnResponse::getOldField() const
{
    return oldField;
}

#pragma endregion


#pragma region ActivateMagicMessage

QString ActivateMagicMessage::TYPE = "ActivateMagicMessage";

ActivateMagicMessage::ActivateMagicMessage(QString fieldMagic)
{
    this->fieldMagic = fieldMagic;
    this->remoteDice = -1;
}

ActivateMagicMessage::ActivateMagicMessage(QString fieldMagic, qint32 remoteDiceNumber)
{
    this->fieldMagic = fieldMagic;
    this->remoteDice = remoteDiceNumber;

}

ActivateMagicMessage::ActivateMagicMessage(const QJsonObject &json)
{
    this->fieldMagic = json["fieldMagic"].toString();
    this->remoteDice = json["remoteDice"].toInteger();
}
QString ActivateMagicMessage::getFieldMagic() const
{
    return this->fieldMagic;
}

qint32 ActivateMagicMessage::getRemoteDice() const
{
    return this->remoteDice;
}

QJsonObject ActivateMagicMessage::toJson()
{
    QJsonObject json;
    json["type"] = ActivateMagicMessage::TYPE;
    json["fieldMagic"] = this->fieldMagic;
    json["remoteDice"] = this->remoteDice;
    return json;
}

#pragma endregion


#pragma region ActivateMagicResponse

QString ActivateMagicResponse::TYPE = "ActivateMagicResponse";

ActivateMagicResponse::ActivateMagicResponse(Color color, QString fieldName, int remainingMagicNumber)
{
    this->color = color;
    this->remainingMagicNumber = remainingMagicNumber;
    this->fieldName = fieldName;
    this->newDiceNumber = 0;
    this->success = true;
    this->broadcast = true;
}

ActivateMagicResponse::ActivateMagicResponse(Color color, QString fieldName, int remainingMagicNumber, int newDiceNumber)
{
    this->color = color;
    this->remainingMagicNumber = remainingMagicNumber;
    this->fieldName = fieldName;
    this->newDiceNumber = newDiceNumber;
    this->success = true;
    this->broadcast = true;
}

ActivateMagicResponse::ActivateMagicResponse(Color color, QString errorMessage)
{
    this->color = color;
    this->errorMessage = errorMessage;
    this->success = false;
    this->broadcast = false;
}

ActivateMagicResponse::ActivateMagicResponse(const QJsonObject &json)
{
    this->color = static_cast<Color>(json["color"].toInteger());
    this->remainingMagicNumber = json["remainingMagicNumber"].toInteger();
    this->fieldName = json["fieldName"].toString();
    this->newDiceNumber = json["newDiceNumber"].toInteger();
    this->success = json["success"].toBool();
    this->errorMessage = json["errorMessage"].toString();
    this->broadcast = json["broadcast"].toBool();
}
int ActivateMagicResponse::getNewDiceNumber() const
{
    return newDiceNumber;
}

Color ActivateMagicResponse::getColor() const
{
    return color;
}

QString ActivateMagicResponse::getFieldName() const
{
    return fieldName;
}

int ActivateMagicResponse::getRemainingMagicNumber() const
{
    return remainingMagicNumber;
}

QJsonObject ActivateMagicResponse::toJson()
{
    QJsonObject json;
    json["type"] = ActivateMagicResponse::TYPE;
    json["color"] = this->color;
    json["remainingMagicNumber"] = this->remainingMagicNumber;
    json["newDiceNumber"] = this->newDiceNumber;
    json["fieldName"] = this->fieldName;
    json["success"] = this->success;
    json["broadcast"] = this->broadcast;
    json["errorMessage"] = this->errorMessage;
    return json;
}
#pragma endregion
