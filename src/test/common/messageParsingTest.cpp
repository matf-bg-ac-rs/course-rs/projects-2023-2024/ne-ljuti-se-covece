#include "../catch.hpp"
#include "../../common/messagefactory.h"
#include "../../common/message.h"

TEST_CASE("Factory", "[factory]"){

    SECTION("CreateGameMessage parsiranje", "[CreateGameMessage]")
    {
        CreateGameMessage* cgm = new CreateGameMessage(4);

        Message* message = MessageFactory::createMessage(cgm->prepareMessage());

        CreateGameMessage* parsedCGM = dynamic_cast<CreateGameMessage*>(message);
        REQUIRE(parsedCGM != nullptr);
        REQUIRE(parsedCGM->numberOfPlayers == 4);
    }

    SECTION("CreateGameResponse parsiranje - uspesan odgovor", "[CreateGameResponse]")
    {
        CreateGameResponse* cgr = new CreateGameResponse("ABCDEFG", Color::Blue);

        Message* message = MessageFactory::createMessage(cgr->prepareMessage());

        CreateGameResponse* parsedCGR = dynamic_cast<CreateGameResponse*>(message);
        REQUIRE(parsedCGR != nullptr);
        REQUIRE(parsedCGR->getGameCode() == "ABCDEFG");
        REQUIRE(parsedCGR->getColor() == Color::Blue);
        REQUIRE(parsedCGR->getSuccess() == true);
    }

    SECTION("CreateGameResponse parsiranje - negativan odgovor ", "[CreateGameResponse]")
    {
        CreateGameResponse* cgr = new CreateGameResponse("Neki error");

        Message* message = MessageFactory::createMessage(cgr->prepareMessage());

        CreateGameResponse* parsedCGR = dynamic_cast<CreateGameResponse*>(message);
        REQUIRE(parsedCGR != nullptr);
        REQUIRE(parsedCGR->getErrorMessage() == "Neki error");
        REQUIRE(parsedCGR->getSuccess() == false);
    }

    SECTION("JoinGameMessage parsiranje", "[JoinGameMessage]")
    {
        JoinGameMessage* jgm = new JoinGameMessage("ABCDEFG");

        Message* message = MessageFactory::createMessage(jgm->prepareMessage());

        JoinGameMessage* parsedJGM = dynamic_cast<JoinGameMessage*>(message);
        REQUIRE(parsedJGM != nullptr);
        REQUIRE(parsedJGM->getGameCode() == "ABCDEFG");
    }


    SECTION("JoinGameResponse parsiranje - uspesan odgovor", "[JoinGameResponse]")
    {
        JoinGameResponse* jgr = new JoinGameResponse(true, Color::Blue, "");

        Message* message = MessageFactory::createMessage(jgr->prepareMessage());

        JoinGameResponse* parsedCGR = dynamic_cast<JoinGameResponse*>(message);
        REQUIRE(parsedCGR != nullptr);
        REQUIRE(parsedCGR->getJoined() == true);
        REQUIRE(parsedCGR->getColor() == Color::Blue);
        REQUIRE(parsedCGR->getSuccess() == true);
        REQUIRE(parsedCGR->getErrorMessage() == "");
    }

    SECTION("JoinGameResponse parsiranje - neuspesan odgovor", "[JoinGameResponse]")
    {
        JoinGameResponse* jgr = new JoinGameResponse(false, Color::Blue, "Neki error");

        Message* message = MessageFactory::createMessage(jgr->prepareMessage());

        JoinGameResponse* parsedCGR = dynamic_cast<JoinGameResponse*>(message);
        REQUIRE(parsedCGR != nullptr);
        REQUIRE(parsedCGR->getJoined() == false);
        REQUIRE(parsedCGR->getColor() == Color::Blue);
        REQUIRE(parsedCGR->getSuccess() == false);
        REQUIRE(parsedCGR->getErrorMessage() == "Neki error");
    }

    SECTION("PlayerReadyMessage parsiranje", "[PlayerReadyMessage]")
    {
        PlayerReadyMessage* prm = new PlayerReadyMessage();

        Message* message = MessageFactory::createMessage(prm->prepareMessage());

        PlayerReadyMessage* parsedPRM = dynamic_cast<PlayerReadyMessage*>(message);
        REQUIRE(parsedPRM != nullptr);
    }

    SECTION("PlayerReadyResponse parsiranje - uspesan odgovor", "[PlayerReadyResponse]")
    {
        PlayerReadyResponse* prr = new PlayerReadyResponse(Color::Blue, true, "");

        Message* message = MessageFactory::createMessage(prr->prepareMessage());

        PlayerReadyResponse* parsedPRR = dynamic_cast<PlayerReadyResponse*>(message);
        REQUIRE(parsedPRR != nullptr);
        REQUIRE(parsedPRR->getColor() == Color::Blue);
        REQUIRE(parsedPRR->getSuccess() == true);
        REQUIRE(parsedPRR->getErrorMessage() == "");
    }

    SECTION("PlayerReadyResponse parsiranje - neuspesan odgovor", "[PlayerReadyResponse]")
    {
        PlayerReadyResponse* prr = new PlayerReadyResponse(Color::Blue, false, "Neki error");

        Message* message = MessageFactory::createMessage(prr->prepareMessage());

        PlayerReadyResponse* parsedPRR = dynamic_cast<PlayerReadyResponse*>(message);
        REQUIRE(parsedPRR != nullptr);
        REQUIRE(parsedPRR->getColor() == Color::Blue);
        REQUIRE(parsedPRR->getSuccess() == false);
        REQUIRE(parsedPRR->getErrorMessage() == "Neki error");
    }

    SECTION("StartGameMessage parsiranje", "[StartGameMessage]")
    {
        StartGameMessage* sgm = new StartGameMessage();

        Message* message = MessageFactory::createMessage(sgm->prepareMessage());

        StartGameMessage* parsedSGM = dynamic_cast<StartGameMessage*>(message);
        REQUIRE(parsedSGM != nullptr);
    }

    SECTION("StartGameResponse parsiranje - uspesan odgovor", "[StartGameResponse]")
    {
        StartGameResponse* sgr = new StartGameResponse(Color::Blue, true, "");

        Message* message = MessageFactory::createMessage(sgr->prepareMessage());

        StartGameResponse* parsedSGR = dynamic_cast<StartGameResponse*>(message);
        REQUIRE(parsedSGR != nullptr);
        REQUIRE(parsedSGR->getColor() == Color::Blue);
        REQUIRE(parsedSGR->getSuccess() == true);
        REQUIRE(parsedSGR->getErrorMessage() == "");
    }

    SECTION("StartGameResponse parsiranje - neuspesan odgovor", "[StartGameResponse]")
    {
        StartGameResponse* sgr = new StartGameResponse(Color::Blue, false, "Neki error");

        Message* message = MessageFactory::createMessage(sgr->prepareMessage());

        StartGameResponse* parsedSGR = dynamic_cast<StartGameResponse*>(message);
        REQUIRE(parsedSGR != nullptr);
        REQUIRE(parsedSGR->getColor() == Color::Blue);
        REQUIRE(parsedSGR->getSuccess() == false);
        REQUIRE(parsedSGR->getErrorMessage() == "Neki error");
    }

    SECTION("RollDiceMessage parsiranje", "[RollDiceMessage]")
    {
        RollDiceMessage* rdm = new RollDiceMessage();

        Message* message = MessageFactory::createMessage(rdm->prepareMessage());

        RollDiceMessage* parsedRDM = dynamic_cast<RollDiceMessage*>(message);
        REQUIRE(parsedRDM != nullptr);
    }

    SECTION("RollDiceResponse parsiranje - uspesan odgovor", "[RollDiceResponse]")
    {
        RollDiceResponse* rdr = new RollDiceResponse(6, Color::Blue);

        Message* message = MessageFactory::createMessage(rdr->prepareMessage());

        RollDiceResponse* parsedRDR = dynamic_cast<RollDiceResponse*>(message);
        REQUIRE(parsedRDR != nullptr);
        REQUIRE(parsedRDR->getColor() == Color::Blue);
        REQUIRE(parsedRDR->getSuccess() == true);
        REQUIRE(parsedRDR->getNumber() == 6);
    }

    SECTION("RollDiceResponse parsiranje - neuspesan odgovor", "[StartGameResponse]")
    {
        RollDiceResponse* rdr = new RollDiceResponse(Color::Blue, "Neki error");

        Message* message = MessageFactory::createMessage(rdr->prepareMessage());

        RollDiceResponse* parsedSGR = dynamic_cast<RollDiceResponse*>(message);
        REQUIRE(parsedSGR != nullptr);
        REQUIRE(parsedSGR->getColor() == Color::Blue);
        REQUIRE(parsedSGR->getSuccess() == false);
        REQUIRE(parsedSGR->getErrorMessage() == "Neki error");
    }

    SECTION("EndTurnMessage parsiranje", "[EndTurnMessage]")
    {
        EndTurnMessage* etm = new EndTurnMessage();

        Message* message = MessageFactory::createMessage(etm->prepareMessage());

        EndTurnMessage* parsedETM = dynamic_cast<EndTurnMessage*>(message);
        REQUIRE(parsedETM != nullptr);
    }

    SECTION("EndTurnResponse parsiranje - uspesan odgovor", "[EndTurnResponse]")
    {
        EndTurnResponse* etr = new EndTurnResponse(Color::Blue, true, "");

        Message* message = MessageFactory::createMessage(etr->prepareMessage());

        EndTurnResponse* parsedRDR = dynamic_cast<EndTurnResponse*>(message);
        REQUIRE(parsedRDR != nullptr);
        REQUIRE(parsedRDR->getColor() == Color::Blue);
        REQUIRE(parsedRDR->getSuccess() == true);
        REQUIRE(parsedRDR->getErrorMessage() == "");
    }

    SECTION("EndTurnResponse parsiranje - neuspesan odgovor", "[EndTurnResponse]")
    {
        EndTurnResponse* etr = new EndTurnResponse(Color::Blue, false, "Neki error");

        Message* message = MessageFactory::createMessage(etr->prepareMessage());

        EndTurnResponse* parsedSGR = dynamic_cast<EndTurnResponse*>(message);
        REQUIRE(parsedSGR != nullptr);
        REQUIRE(parsedSGR->getColor() == Color::Blue);
        REQUIRE(parsedSGR->getSuccess() == false);
        REQUIRE(parsedSGR->getErrorMessage() == "Neki error");
    }
}
