#ifndef CLIENT_H
#define CLIENT_H

#include <QObject>
#include <QTcpSocket>
#include <QDebug>
#include "../../common/message.h"
#include "clientmanagers.h"
#include "qthread.h"

class Client : public QThread
{
    Q_OBJECT

public:
    Color color;

    Response* sendWithResponse(Message &message);
    void send(Message &message);
    void connectToServer() const;
    static Client* getInstance();
    void run() override;
    bool getBackgroundThreadStarted() const;

signals:

public slots:
    void readResponse();
    void disconnected();

private:
    explicit Client(QObject *parent = nullptr);

    static Client* instance;
    bool backgroundThreadStarted;
    QTcpSocket *socket;
    ClientMessageManager* messageManager;
};

#endif // CLIENT_H
