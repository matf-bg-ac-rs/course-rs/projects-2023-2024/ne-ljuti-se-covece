#ifndef MANAGERS_H
#define MANAGERS_H

#include <QObject>
#include <QTcpSocket>
#include <QVector>
#include "../common/message.h"
#include "../common/turncontext.h"
#include "board.h"

class BaseParticipant;
class ChainElement;
class Bot;

class GameManager : public QObject
{
    Q_OBJECT
public:
    explicit GameManager(QString gameCode, QObject *parent = nullptr);

    Board* board;
    QString gameCode;
    bool playerReadyList[4];

    void broadcastResponse(Message *message);
    Color joinGame(BaseParticipant* thread);
    int rollDice();
    QPair<QString, QString> movePawn(QString fieldName, Color color);
    bool isValidMove(QString fieldName, Color color);
    bool playerIsReady(Color color);
    bool startGame(Color color);
    Color nextPlayer(Color color);
    Color shouldEndGame();
    bool hasValidMoves(Color playerColor);
    void decreaseShieldMagic();
    bool isMagicAvailable(QString magicName) const;
    void useMagic(QString magicName, qint32 remoteDice = -1);
    int getNumberOfRemainingMagic(QString magicName) const;
    TurnContext *getTurnContext() const;

    bool isGameStarted() const;

    QList<BaseParticipant *> getParticepants() const;

private:
    QList<BaseParticipant*> particepants;
    QTcpSocket* socket;
    int turnCount;
    bool gameStarted;
    TurnContext* turnContext;

signals:
    void sendMessage(QByteArray message);

};

class MessageManager : public QObject
{
    Q_OBJECT
public:
    explicit MessageManager(QObject *parent = nullptr);

    Response* handleMessage(const QByteArray &rawMessage, BaseParticipant *participant);

    ChainElement *getChainElement() const;

private:
    ChainElement* chainElement;

signals:

};
#endif // MANAGERS_H
