#ifndef ENDGAME_H
#define ENDGAME_H

#include <QWidget>
#include <QGraphicsScene>
#include "../client/client.h"


namespace Ui {
class endGame;
}

class endGame : public QWidget
{
    Q_OBJECT

public:
    ~endGame();
    static endGame* getInstance();
    void showWinner(Color color);

private:
    static endGame* instance;
    Ui::endGame *ui;
    explicit endGame(QWidget *parent = nullptr);
};

#endif // ENDGAME_H
