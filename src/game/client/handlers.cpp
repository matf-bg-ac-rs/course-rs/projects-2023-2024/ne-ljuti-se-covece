#include "handlers.h"
#include "../view/mainwindow.h"
#include "../view/endgame.h"

ClientChainElement::ClientChainElement(ClientChainElement* nextElement)
    : QObject{nextElement}
{
    this->nextElement = nextElement;
};

QString ClientChainElement::enumValue(Color color){

    switch (color) {
    case 0:
        return "BLUE";
    case 1:
        return "RED";
    case 2:
        return "GREEN";
    case 3:
        return "YELLOW";
    default:
        return "INVALID";
    }
}

void DefaultResponseHandler::handleResponse(Response* response)
{
    throw std::runtime_error("DefaultHandler ne treba da se koristi, nije implementiran handler za poruku" + response->prepareMessage());
}

void CreateGameResponseHandler::handleResponse(Response* response)
{
    CreateGameResponse* cgr = dynamic_cast<CreateGameResponse*>(response);

    // Proveriti da li je message instanca klase CreateGameResponse
    if (cgr)
    {
        qDebug() << "Starting CreateGameResponseHandler..";


        MainWindow::getInstance()->transitionToCodePage(cgr->getGameCode());
        Client::getInstance()->start();
        Client::getInstance()->color = cgr->getColor();
        return;
    }

    // Called C++ object pointer is null - Validno je da cgr bude null ukoliko je dobijen neodgovarajuci response,
    // ali response uvek mora da postoji jer ga pravi MessageFactory.
    QMessageBox::critical(new QWidget(), "Internal server error", "Server returned unexpected response: " + response->prepareMessage());
}

void JoinGameResponseHandler::handleResponse(Response* response)
{
    JoinGameResponse* jgr = dynamic_cast<JoinGameResponse*>(response);

    // Proveriti da li je message instanca klase JoinGameResponse
    if (jgr)
    {
        qDebug() << "Starting JoinGameResponseHandler..";

        if(jgr->getJoined())
        {
            // Startuj background thread samo ako si kliknuo na dugme
            if (!Client::getInstance()->getBackgroundThreadStarted()){
                Client::getInstance()->start();
                Client::getInstance()->color = jgr->getColor();

                QMessageBox::information(new QWidget(), "Join Game Response", "Uspesno si se pridruzio igri!");
            }
        }
        else
        {
            QMessageBox::critical(new QWidget(), "Join Game Error", jgr->getErrorMessage());
        }
        return;
    }

    // Ukoliko nije, proslediti poruku dalje u chain
    return this->nextElement->handleResponse(response);
}


void PlayerReadyResponseHandler::handleResponse(Response* response)
{
    PlayerReadyResponse* prr = dynamic_cast<PlayerReadyResponse*>(response);

    // Proveriti da li je message instanca klase PlayerReadyResponse
    if(prr)
    {
        qDebug() << "Starting PlayerReadyResponseHandler..";
        if (prr->getSuccess())
        {
            if(prr->getColor() == Client::getInstance()->color)
            {
                MainWindow::getInstance()->transitionToLoadingPage();
            }
        }
        else
        {
            QMessageBox::critical(new QWidget(), "Error", prr->getErrorMessage());
        }
        return;
    }

    // Ukoliko nije, proslediti poruku dalje u chain
    return this->nextElement->handleResponse(response);
}

void StartGameResponseHandler::handleResponse(Response* response)
{
    StartGameResponse* sgr = dynamic_cast<StartGameResponse*>(response);

    // Proveriti da li je message instanca klase StartGameResponse
    if (sgr)
    {
        qDebug() << "Starting StartGameResponseHandler..";

        if(sgr->getSuccess())
        {
            MainGame::getInstance()->show();
            MainGame::getInstance()->setPlayerColor(Client::getInstance()->color);
            MainWindow::getInstance()->hide();
        }
        else
        {
            QMessageBox::information(new QWidget(), "Start game response!", sgr->getErrorMessage());
        }

        return;
    }

    // Ukoliko nije, proslediti poruku dalje u chain
    return this->nextElement->handleResponse(response);
}

void EndTurnResponseHandler::handleResponse(Response* response)
{
    EndTurnResponse* etr = dynamic_cast<EndTurnResponse*>(response);

    // Proveriti da li je message instanca klase EndTurnResponse
    if (etr)
    {
        qDebug() << "Starting EndTurnResponseHandler..";

        if(etr->getErrorMessage().isEmpty()){

            QString info = this->enumValue(etr->getColor()) + " player turn.";
            MainGame::getInstance()->updateInfoLabel(info);
            MainGame::getInstance()->removeShieldMagic(etr->getColor());
        }
        else
        {
            QString info = etr->getErrorMessage();
            MainGame::getInstance()->updateInfoLabel(info);
        }

        // TODO: Verovatno treba da disablujemo dugmice ako vise nismo na potezu
        qDebug() << "EndTurnHandler zavrsava";
        return;
    }

    // Ukoliko nije, proslediti poruku dalje u chain
    return this->nextElement->handleResponse(response);
}


void RollDiceResponseHandler::handleResponse(Response* response)
{
    RollDiceResponse* rdr = dynamic_cast<RollDiceResponse*>(response);

    // Proveriti da li je message instanca klase RollDiceResponse
    if (rdr)
    {
        qDebug() << "Starting RollDiceResponseHandler..";

        if(rdr->getSuccess())
        {
            MainGame::getInstance()->changeDiceValue(rdr->getNumber());
            QString info = this->enumValue(rdr->getColor()) + " rolled " + QString::number(rdr->getNumber());
            MainGame::getInstance()->updateInfoLabel(info);

        }
        else
        {
            QString info = rdr->getErrorMessage();
            MainGame::getInstance()->updateInfoLabel(info);
        }

        return;
    }

    // Ukoliko nije, proslediti poruku dalje u chain
    return this->nextElement->handleResponse(response);
}

void EndGameResponseHandler::handleResponse(Response* response)
{
    EndGameResponse* egr = dynamic_cast<EndGameResponse*>(response);

    // Proveriti da li je message instanca klase EndGameResponse
    if (egr)
    {
        qDebug() << "Starting EndGameResponseHandler..";
        endGame::getInstance()->show();
        endGame::getInstance()->showWinner(egr->getColor());
        MainGame::getInstance()-> ~MainGame();
        return;    
    }

    // Ukoliko nije, proslediti poruku dalje u chain
    return this->nextElement->handleResponse(response);
}

void MovePawnResponseHandler::handleResponse(Response* response)
{
    MovePawnResponse* mpr = dynamic_cast<MovePawnResponse*>(response);

    // Proveriti da li je message instanca klase MovePawnResponse
    if (mpr)
    {
        qDebug() << "Starting MovePawnResponseHandler..";
        if(mpr->getSuccess()) {

            MainGame::getInstance()->changeToTransparent(mpr->getOldField());
            if ( mpr->getNewField() != "finished" )
            {
                MainGame::getInstance()->movePawn(mpr->getColor(), mpr->getNewField());
            }
            if (mpr->getEatenColor() != Color::Invalid )
            {
                MainGame::getInstance()->movePawn(mpr->getEatenColor(), mpr->getEatenPawnNewField());
            }
        }
        else
        {
            QString info = mpr->getErrorMessage();
            MainGame::getInstance()->updateInfoLabel(info);
        }

        return;
    }
    return this->nextElement->handleResponse(response);
}

void ActivateMagicResponseHandler::handleResponse(Response* response)
{
    ActivateMagicResponse* amr = dynamic_cast<ActivateMagicResponse*>(response);

    // Proveriti da li je message instanca klase ActivateMagicResponse
    if (amr)
    {
        qDebug() << "Starting ActivateMagicResponseHandler..";
        if (amr->getSuccess())
        {

            QString info = this->enumValue(amr->getColor()) + " iskoristio magiju " + amr->getFieldName();
            MainGame::getInstance()->updateInfoLabel(info);

            if( amr->getNewDiceNumber() >= 1  && amr->getNewDiceNumber() <= 6)
            {
                MainGame::getInstance()->changeDiceValue(amr->getNewDiceNumber());
            }
            if(amr->getColor() == Client::getInstance()->color)
            {
                int remainingMagic = amr->getRemainingMagicNumber();
                QString pressedBttn = amr->getFieldName();
                MainGame::getInstance()->updateMagicNumber(pressedBttn, remainingMagic);

            }
            if(amr->getFieldName() == "shield")
            {
                MainGame::getInstance()->placeShieldMagic(amr->getColor());
            }
        }
        else
        {
            QString info = amr->getErrorMessage();
            MainGame::getInstance()->updateInfoLabel(info);
        }
        return ;
    }

    // Ukoliko nije, proslediti poruku dalje u chain
    return this->nextElement->handleResponse(response);
}
