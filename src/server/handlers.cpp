#include "handlers.h"
#include "managers.h"
#include "server.h"
#include "participants.h"

ChainElement::ChainElement(ChainElement* nextElement)
    : QObject{nextElement}
{
    this->nextElement = nextElement;
};

Response* DefaultHandler::handleMessage(Message* message, BaseParticipant *participant)
{
    throw std::runtime_error("DefaultHandler ne treba da se koristi, nije implementiran handler za poruku" + message->prepareMessage());
}

Response* CreateGameMessageHandler::handleMessage(Message* message, BaseParticipant *participant)
{
    CreateGameMessage* cgm = dynamic_cast<CreateGameMessage*>(message);
    // Proveriti da li je message instanca klase CreateGameMessageHandler
    if (cgm)
    {
        qDebug() << "Starting CreateGameMessageHandler..";
        if(participant->gameManager != nullptr)
        {
            return new CreateGameResponse("Partija vec napravljena! Ne mozete ponovo kreirati partiju.");
        }
        if(cgm->numberOfPlayers < 1 || cgm->numberOfPlayers > 4)
        {
            return new CreateGameResponse("Nevalidan broj igraca za partiju.");
        }

        participant->gameManager = Server::getInstance()->generateNewGame();
        Color joinedColor = participant->gameManager->joinGame(participant);
        participant->color = joinedColor;

        connect(participant->gameManager, &GameManager::sendMessage, participant, &BaseParticipant::sendMessage);

        // Instanciranje botova
        int botCount = 4 - cgm->getNumberOfPlayers();
        Color lastJoinedColor = participant->color;

        while(botCount--)
        {
            Bot *bot = new Bot(participant->gameManager, participant->gameManager->nextPlayer(lastJoinedColor));
            participant->gameManager->joinGame(bot);
            bot->prepareReadyStatus();
            bot->moveToThread(bot);
            bot->start();

            lastJoinedColor = bot->getColor();
        }

        return new CreateGameResponse(participant->gameManager->gameCode, joinedColor);
    }

    // Ukoliko nije, proslediti poruku dalje u chain
    return this->nextElement->handleMessage(message, participant);
}

Response* JoinGameMessageHandler::handleMessage(Message* message, BaseParticipant *participant)
{
    JoinGameMessage* jgm = dynamic_cast<JoinGameMessage*>(message);

    // Proveriti da li je message instanca klase JoinGameMessageHandler
    if (jgm)
    {
        qDebug() << "Starting JoinGameMessageHandler..";

        if(participant->gameManager != nullptr)
        {
            return new JoinGameResponse(false /*joined*/, Color::Invalid, "Vec si pridruzen partiji!");
        }

        participant->gameManager = Server::getInstance()->joinGame(jgm->getGameCode());
        bool gameExists = participant->gameManager != nullptr;

        if(!gameExists){
            return new JoinGameResponse(false /*joined*/, Color::Invalid, "Partija ne postoji!");
        }

        Color joinedColor = participant->gameManager->joinGame(participant);
        if(joinedColor == Color::Invalid){
            return new JoinGameResponse(false /*joined*/, Color::Invalid, "Partija je puna!");
        }

        participant->color = joinedColor;
        connect(participant->gameManager, &GameManager::sendMessage, participant, &BaseParticipant::sendMessage);

        return new JoinGameResponse(true /*joined*/, joinedColor, "");
    }

    // Ukoliko nije, proslediti poruku dalje u chain
    return this->nextElement->handleMessage(message, participant);
}

Response* PlayerReadyMessageHandler::handleMessage(Message* message, BaseParticipant *participant)
{
    PlayerReadyMessage* prm = dynamic_cast<PlayerReadyMessage*>(message);

    // Proveriti da li je message instanca klase PlayerReadyMessageHandler
    if (prm)
    {
        if(participant->gameManager == nullptr)
        {
            return new PlayerReadyResponse(participant->color, false, "Niste povezani u partiju!");
        }

        qDebug() << "Starting PlayerReadyMessageHandler..";
        bool success = participant->gameManager->playerIsReady(participant->color);
        QString errorMessage = !success ? "Igrac je vec spreman" : "";
        return new PlayerReadyResponse(participant->color, success, errorMessage);
    }

    // Ukoliko nije, proslediti poruku dalje u chain
    return this->nextElement->handleMessage(message, participant);
}

Response* StartGameMessageHandler::handleMessage(Message* message, BaseParticipant *participant)
{
    StartGameMessage* sgm = dynamic_cast<StartGameMessage*>(message);

    // Proveriti da li je message instanca klase StartGameMessageHandler
    if (sgm)
    {
        qDebug() << "Starting StartGameMessageHandler..";

        if (participant->gameManager->isGameStarted()){
            return new StartGameResponse(participant->color, false, "Igra je vec startovana, ne moze se ponovo startovati!");
        }

        bool success = participant->gameManager->startGame(participant->color);
        QString errorMessage = !success ? "Nisu ispunjeni uslovi za startovanje igre" : "";
        if(success){
            participant->gameManager->getTurnContext()->reset();
            participant->gameManager->getTurnContext()->setCurrentPlayerColor(participant->color);
        }
        return new StartGameResponse(participant->color, success, errorMessage);
    }

    // Ukoliko nije, proslediti poruku dalje u chain
    return this->nextElement->handleMessage(message, participant);
}

Response* EndTurnMessageHandler::handleMessage(Message* message, BaseParticipant *participant)
{
    EndTurnMessage* egm = dynamic_cast<EndTurnMessage*>(message);

    // Proveriti da li je message instanca klase EndTurnMessageHandler
    if (egm)
    {
        qDebug() << "Starting EndTurnMessageHandler..";

        if(participant->color != participant->gameManager->getTurnContext()->getCurrentPlayerColor()){
            return new EndTurnResponse(participant->color, false /* success */, "Niste na potezu, molimo sacekajte trenutnog igraca!");
        }

        if(participant->gameManager->getTurnContext()->getRemainingNumberOfRolles() != 0)
        {
            return new EndTurnResponse(participant->color, false /* success */, "Morate baciti kockicu!");
        }

        if(participant->gameManager->getTurnContext()->getRemainingNumberOfMoves() != 0 &&
            participant->gameManager->hasValidMoves(participant->color) &&
            participant->gameManager->getTurnContext()->getLastRolledValue() != 0)
        {
            return new EndTurnResponse(participant->color, false /* success */, "Imate slobodan potez, morate pomeriti figuricu!");
        }

        Color nextPlayer = participant->gameManager->nextPlayer(participant->color);
        participant->gameManager->decreaseShieldMagic();
        participant->gameManager->getTurnContext()->reset();
        participant->gameManager->getTurnContext()->setCurrentPlayerColor(nextPlayer);

        if( participant->gameManager->shouldEndGame() != Color::Invalid )
            return new EndGameResponse( participant->gameManager->shouldEndGame() );

        return new EndTurnResponse(nextPlayer);
    }

    // Ukoliko nije, proslediti poruku dalje u chain
    return this->nextElement->handleMessage(message, participant);
}

Response* RollDiceMessageHandler::handleMessage(Message* message, BaseParticipant *participant)
{
    RollDiceMessage* rdm = dynamic_cast<RollDiceMessage*>(message);

    // Proveriti da li je message instanca klase RollDiceMessage
    if (rdm)
    {
        if(participant->color != participant->gameManager->getTurnContext()->getCurrentPlayerColor())
        {
            return new RollDiceResponse(participant->color, "Ne mozete baciti kockicu, jer niste na potezu!");
        }

        if(participant->gameManager->getTurnContext()->getRemainingNumberOfRolles() == 0)
        {
            return new RollDiceResponse(participant->color, "Ispunili ste dozvoljen broj bacanja kockice!");
        }
        return new RollDiceResponse(participant->gameManager->rollDice(), participant->color);
    }

    // Ukoliko nije, proslediti poruku dalje u chain
    return this->nextElement->handleMessage(message, participant);
}

Response* MovePawnMessageHandler::handleMessage(Message* message, BaseParticipant *participant)
{
    MovePawnMessage* mpm = dynamic_cast<MovePawnMessage*>(message);

    // Proveriti da li je message instanca klase MovePawnMessage
    if (mpm)
    {
        if(participant->color != mpm->pawnColor)
        {
            return new MovePawnResponse(participant->color, "Ne mozete pomeriti figuricu jer nije vasa!");
        }

        if(participant->gameManager->getTurnContext()->getRemainingNumberOfMoves() == 0)
        {
            return new MovePawnResponse(participant->color, "Vec ste pomerili figuricu u ovom krugu!");
        }

        if(participant->gameManager->getTurnContext()->getRemainingNumberOfRolles() != 0 &&
            participant->gameManager->getTurnContext()->getLastRolledValue() == 0)
        {
            return new MovePawnResponse(participant->color, "Morate baciti kockicu pre pomeranja figure!");
        }

        QString fieldName = mpm->field;
        if(!participant->gameManager->isValidMove(fieldName, participant->color))
        {
            return new MovePawnResponse(participant->color, "Ne mozete pomeriri pijuna, nevalidan potez!");
        }

        QPair<QString, QString> pair = participant->gameManager->movePawn(fieldName, participant->color);
        QString impactedField = pair.first;
        QString eatenPawnNewField = pair.second;
        if (eatenPawnNewField.isEmpty() )
        {
            return new MovePawnResponse(mpm->field, participant->color, impactedField, eatenPawnNewField, Color::Invalid);
        }
        else
        {
            Color eatenColor = Color::Invalid;
            if ( eatenPawnNewField.front() == 'b' )
                eatenColor = Color::Blue;
            else if ( eatenPawnNewField.front() == 'r' )
                eatenColor = Color::Red;
            else if ( eatenPawnNewField.front() == 'g' )
                eatenColor = Color::Green;
            else if ( eatenPawnNewField.front() == 'y' )
                eatenColor = Color::Yellow;

            return new MovePawnResponse(mpm->field, participant->color, impactedField, eatenPawnNewField, eatenColor);
        }
    }

    // Ukoliko nije, proslediti poruku dalje u chain
    return this->nextElement->handleMessage(message, participant);
}

Response* ActivateMagicMessageHandler::handleMessage(Message* message, BaseParticipant *participant)
{
    ActivateMagicMessage* amm = dynamic_cast<ActivateMagicMessage*>(message);

    // Proveriti da li je message instanca klase ActivateMagicMessage
    if (amm)
    {
        QString magicName = amm->getFieldMagic();

        if (participant->color != participant->gameManager->getTurnContext()->getCurrentPlayerColor()) {
            return new ActivateMagicResponse(participant->color, "Ne mozete koristiti magiju, jer nije vas potez!");
        }
        if (!participant->gameManager->isMagicAvailable(magicName)) {
            return new ActivateMagicResponse(participant->color, "Nemate ovu magiju na raspolaganju!");
        }
        if(participant->gameManager->getTurnContext()->getRemainingNumberOfRolles() == 0  && magicName=="remote_dice" )
        {
            return new ActivateMagicResponse(participant->color, "Ne mozete iskoristi ovu magiju nakon bacanja kockice!");
        }
        if(participant->gameManager->getTurnContext()->getRemainingNumberOfRolles() > 0  && (
                magicName == "plus_1" || magicName == "minus_1"  || magicName == "double_dice" ) )
        {
            return new ActivateMagicResponse(participant->color, "Ne mozete iskoristi ovu magiju pre bacanje kockice!");
        }
        if (participant->gameManager->getTurnContext()->getNumberOfMagics() > 0 )
        {
            return new ActivateMagicResponse(participant->color, "Ne mozete koristiti vise magija u jednom potezu!");
        }
        participant->gameManager->getTurnContext()->increseNumberOfMagics();


        int remoteDiceNumber = amm->getRemoteDice();
        if (remoteDiceNumber != -1 && magicName == "remote_dice") {
            participant->gameManager->getTurnContext()->useOneRoll();
            participant->gameManager->useMagic(magicName, remoteDiceNumber);
        } else {
            participant->gameManager->useMagic(magicName);
        }
        if (magicName == "plus_1" || magicName == "minus_1" || magicName=="remote_dice" )
        {
            return new ActivateMagicResponse(participant->color, magicName, participant->gameManager->getNumberOfRemainingMagic(magicName), participant->gameManager->getTurnContext()->getLastRolledValue());
        }
        else
        {
            return new ActivateMagicResponse(participant->color, magicName, participant->gameManager->getNumberOfRemainingMagic(magicName));
        }
    }

    // Ukoliko nije, proslediti poruku dalje u chain
    return this->nextElement->handleMessage(message, participant);
}
