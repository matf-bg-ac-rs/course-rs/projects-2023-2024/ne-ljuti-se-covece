#include "pawn.h"

Pawn::Pawn( Color col, Square *square, QObject* parent )
{
    this->square = square;
    this->isHome = true;
    this->isFinished = false;
    this->color = col;
}

Pawn::~Pawn()
{

}

Square *Pawn::getSquare() const
{
    return square;
}
