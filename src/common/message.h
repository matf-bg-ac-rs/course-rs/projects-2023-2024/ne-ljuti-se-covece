#ifndef MESSAGE_H
#define MESSAGE_H

#include <QJsonObject>
#include <QByteArray>
#include "common_global.h"


enum Color{
    Blue,
    Red,
    Green,
    Yellow,
    Invalid,
};


class COMMON_EXPORT Message : public QObject
{
public:
    QByteArray prepareMessage();

private:
    virtual QJsonObject toJson() = 0;

};

class COMMON_EXPORT Response: public Message
{
public:

    QString getErrorMessage() const;
    bool getSuccess() const;
    bool shouldBroadcast() const;

protected:
    QString errorMessage;
    bool broadcast;
    bool success;
};

class COMMON_EXPORT CreateGameMessage : public Message
{
public:
    static QString TYPE;

    CreateGameMessage(int numberOfPlayers);
    CreateGameMessage(const QJsonObject &json);

    int getNumberOfPlayers() const;

    // Ova polja mozda ne treba da budu public, isto i u CreateGameResponse
    int numberOfPlayers;
    QJsonObject toJson() override;
};

class COMMON_EXPORT CreateGameResponse: public Response
{
public:
    static QString TYPE;
    CreateGameResponse(QString gameCode, Color color);
    CreateGameResponse(QString errorMessage);
    CreateGameResponse(const QJsonObject &json);

    Color getColor() const;
    QString getGameCode() const;
    QJsonObject toJson() override;

private:
    QString gameCode;
    Color color;
};

class COMMON_EXPORT JoinGameMessage : public Message
{
public:
    static QString TYPE;

    JoinGameMessage(QString gameCode);
    JoinGameMessage(const QJsonObject &json);

    QString getGameCode() const;

private:
    QString gameCode;
    QJsonObject toJson() override;
};

class COMMON_EXPORT JoinGameResponse: public Response
{
public:
    static QString TYPE;

    JoinGameResponse(bool joined, Color color, QString errorMessage);
    JoinGameResponse(const QJsonObject &json);

    bool getJoined() const;
    Color getColor() const;

private:
    bool joined;
    Color color;

    QJsonObject toJson() override;
};

class COMMON_EXPORT PlayerReadyMessage : public Message
{
public:
    static QString TYPE;

    PlayerReadyMessage();
    PlayerReadyMessage(const QJsonObject &json);

private:
    QJsonObject toJson() override;
};

class COMMON_EXPORT PlayerReadyResponse: public Response
{
public:
    static QString TYPE;

    PlayerReadyResponse(Color color, bool success, QString errorMessage);
    PlayerReadyResponse(const QJsonObject &json);

    Color getColor() const;

private:
    Color color;
    QJsonObject toJson() override;
};

class COMMON_EXPORT StartGameMessage : public Message
{
public:
    static QString TYPE;

    StartGameMessage();
    StartGameMessage(const QJsonObject &json);

private:
    QJsonObject toJson() override;
};

class COMMON_EXPORT StartGameResponse: public Response
{
public:
    static QString TYPE;

    StartGameResponse(Color color, bool success, QString errorMessage);
    StartGameResponse(const QJsonObject &json);

    Color getColor() const;

private:
    Color color;
    QJsonObject toJson() override;
};

class COMMON_EXPORT EndTurnMessage : public Message
{
public:
    static QString TYPE;

    EndTurnMessage();
    EndTurnMessage(const QJsonObject &json);

private:
    QJsonObject toJson() override;
};

class COMMON_EXPORT EndTurnResponse: public Response
{
public:
    static QString TYPE;

    EndTurnResponse(Color color, bool success = true, QString errorMessage = "");
    EndTurnResponse(const QJsonObject &json);

    Color getColor() const;

private:
    Color color;
    QJsonObject toJson() override;
};

class COMMON_EXPORT RollDiceMessage : public Message
{
public:
    static QString TYPE;
    RollDiceMessage();

    RollDiceMessage(const QJsonObject &json);
private:
    QJsonObject toJson() override;
};

class COMMON_EXPORT RollDiceResponse: public Response
{
public:
    static QString TYPE;

    RollDiceResponse(int number, Color color);
    RollDiceResponse(Color color, QString errorMessage);
    RollDiceResponse(const QJsonObject &json);

    int getNumber() const;
    Color getColor() const;

private:
    int number;
    Color color;
    QJsonObject toJson() override;
};

class COMMON_EXPORT EndGameResponse: public Response
{
public:
    static QString TYPE;

    EndGameResponse(Color color);
    EndGameResponse(Color color, QString errorMessage);
    EndGameResponse(const QJsonObject &json);

    Color getColor() const;

private:
    Color color;
    QJsonObject toJson() override;
};

class COMMON_EXPORT MovePawnMessage : public Message
{
public:
    static QString TYPE;

    MovePawnMessage (QString field, Color pawnColor);
    MovePawnMessage (const QJsonObject &json);
    QJsonObject toJson() override;


    QString field;
    Color pawnColor;

};

class COMMON_EXPORT MovePawnResponse: public Response
{
public:
    static QString TYPE;

    MovePawnResponse(QString field, Color color, QString newField, QString eatenPawnNewField, Color eatenColor);
    MovePawnResponse(Color color, QString errorMessage);
    MovePawnResponse(const QJsonObject &json);

    Color getEatenColor() { return eatenColor; }
    QString getEatenPawnNewField() { return eatenPawnNewField; }
    QString getNewField() const ;
    QString getOldField() const;
    Color getColor() const;
    QJsonObject toJson() override;


private:
    Color color;
    Color eatenColor = Color::Invalid;
    QString eatenPawnNewField;
    QString newField;
    QString oldField;

};

class COMMON_EXPORT ActivateMagicMessage : public Message
{
public:
    static QString TYPE;

    ActivateMagicMessage (QString fieldMagic);
    ActivateMagicMessage (QString fieldMagic, qint32 remoteDice);
    ActivateMagicMessage (const QJsonObject &json);
    QString getFieldMagic() const;
    qint32 getRemoteDice() const;

private:

    QString fieldMagic;
    qint32 remoteDice;
    QJsonObject toJson() override;
};

class COMMON_EXPORT ActivateMagicResponse: public Response
{
public:
    static QString TYPE;

    ActivateMagicResponse(Color color, QString fieldName, int remainingMagicNumber);
    ActivateMagicResponse(Color color, QString fieldName, int remainingMagicNumber, int newDiceNumber);
    ActivateMagicResponse(Color color, QString errorMsg);
    ActivateMagicResponse(const QJsonObject &json);

    Color getColor() const;
    int getRemainingMagicNumber() const;
    QString getFieldName() const;
    int getNewDiceNumber() const;

private:
    Color color;
    int remainingMagicNumber;
    int newDiceNumber;
    QString fieldName;
    QJsonObject toJson() override;
};

#endif // MESSAGE_H
