#ifndef SQUARE_H
#define SQUARE_H

#include<QObject>
#include <QString>
#include <QVector>
#include <QRegularExpression>
#include <QRegularExpressionMatch>
#include <QDebug>

#include "../common/message.h"

class Square : public QObject
{
    Q_OBJECT
private:
    QString name;
    Color occupyingPlayerColor;
    int occupyingPawn;

public:
    Square( QString name, QObject* parent = nullptr);
    Square(QString name, Color color, int pawnId, QObject* parent = nullptr);
    ~Square();
    int getPawnIdx() const { return occupyingPawn; }
    QString getName() const { return name; }
    void updateSquare(Color color, int occupyingPawn);
    Color getColor() const { return occupyingPlayerColor; }
};

#endif // SQUARE_H
