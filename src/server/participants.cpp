#include "participants.h"
#include "managers.h"
#include "../common/messagefactory.h"
#include <QRandomGenerator>
#include <QApplication>

BaseParticipant::BaseParticipant(QObject *parent):
    QThread(parent)
{
    this->messageManager = new MessageManager(this);
}

void BaseParticipant::sendResponse(Response* response)
{
    if (this->gameManager != nullptr && response->shouldBroadcast())
    {
        BroadcastingThread* bt = new BroadcastingThread(this->gameManager, response);
        bt->moveToThread(bt);
        bt->start();
    }
    else
    {
        this->sendMessage(response->prepareMessage());
        delete response;
    }
}

ServerThreadParticipant::ServerThreadParticipant(int id, QObject *parent):
    BaseParticipant(parent)
{
    this->socketDescriptor = id;
    this->gameManager = nullptr;
}

ServerThreadParticipant::~ServerThreadParticipant()
{
}

void ServerThreadParticipant::run()
{
    qDebug() << socketDescriptor << "Starting thread with id: " << QThread::currentThreadId();
    socket = new QTcpSocket();

    if(!socket->setSocketDescriptor(this->socketDescriptor))
    {
        emit error(socket->error());
        return;
    }

    connect(socket, SIGNAL(readyRead()), this, SLOT(readyRead()), Qt::DirectConnection);
    connect(socket, SIGNAL(disconnected()), this, SLOT(disconnected()), Qt::DirectConnection);

    qDebug() << socketDescriptor << "Client Connected";
    exec();
}

void ServerThreadParticipant::readyRead()
{
    try {
        QByteArray data = socket->readAll();
        qDebug() << socketDescriptor << " Data in: " << data;
        Response* response = this->messageManager->handleMessage(data, this);
        this->sendResponse(response);
    } catch (...) {
        qDebug() << "Unknown exception";
    }
}

void ServerThreadParticipant::disconnected()
{
    qDebug() << socketDescriptor << " Disconnected";
    socket->deleteLater();
    this->moveToThread(QApplication::instance()->thread());

    exit(0);
}

void ServerThreadParticipant::sendMessage(QByteArray message)
{

    qDebug() << this->color << " : Ljudskom igracu se salje: " << message;

    // Mozda treba lock
    this->socket->write(message);
    this->socket->waitForBytesWritten(1000);
}

Bot::Bot(GameManager* gameManager, Color color, QObject *parent)
    : BaseParticipant{parent}
{
    this->color = color;
    this->gameManager = gameManager;
    this->isMyTurn = false;
}

void Bot::run()
{
    connect(gameManager, &GameManager::sendMessage, this, &BaseParticipant::sendMessage);
    connect(this, SIGNAL(finished()), this, SLOT(deleteLater()));
    exec();
}

void Bot::prepareReadyStatus()
{
    this->gameManager->playerIsReady(this->color);
}

Message* Bot::generateNextMove(Response* response)
{
    this->msleep(500);
    Message* botAction = this->generateMessageOnEndTurnResponse(response);

    if(botAction != nullptr){
        return botAction;
    }

    botAction = this->generateMessageOnRollDiceResponse(response);

    if(botAction != nullptr){
        return botAction;
    }

    return this->generateMessageOnMovePawnResponse(response);
}

void Bot::sendMessage(QByteArray rawMessage)
{

    qDebug() << this->color << ": Got response: " << rawMessage;
    Response* response = (Response*)MessageFactory::createMessage(rawMessage);
    Message* message = generateNextMove(response);

    if(message != nullptr){
        Response* response = this->messageManager->handleMessage(message->prepareMessage(), this);
        this->sendResponse(response);
    }
}

Message* Bot::generateMessageOnEndTurnResponse(Response *response)
{
    EndTurnResponse* etr = dynamic_cast<EndTurnResponse*>(response);

    if(etr)
    {
        if(etr->getColor() == this->color){
            this->isMyTurn = true;
        }
        else
        {
            this->isMyTurn = false;
        }


        if(this->isMyTurn){
            return new RollDiceMessage();
        }
    }

    return nullptr;
}

Message* Bot::generateMessageOnRollDiceResponse(Response *response)
{
    RollDiceResponse* rdr = dynamic_cast<RollDiceResponse*>(response);

    if(rdr && this->isMyTurn){
        Square* square = this->randomlySelectPawn();

        if(!this->hasValidMove()){
            return new EndTurnMessage();
        }
        return new MovePawnMessage(square->getName(), square->getColor());
    }

    return nullptr;
}

Message* Bot::generateMessageOnMovePawnResponse(Response *response)
{
    MovePawnResponse* mpr = dynamic_cast<MovePawnResponse*>(response);

    if(mpr && this->isMyTurn){
        if(mpr->getSuccess() || !this->hasValidMove()){
            return new EndTurnMessage();
        }
        else
        {
            Square* square = this->randomlySelectPawn();
            int lastRolledValue = this->gameManager->getTurnContext()->getLastRolledValue();
            while (!this->gameManager->board->isPawnMoveValid(square->getName(), square->getColor(), lastRolledValue))
            {
                square = this->randomlySelectPawn();
            }

            return new MovePawnMessage(square->getName(), square->getColor());
        }
    }

    return nullptr;
}

Square* Bot::randomlySelectPawn()
{
    int randomPawnIndex = QRandomGenerator::global()->bounded(4);
    return this->gameManager->board->getPlayer(this->getColor())->getPawnByIdx(randomPawnIndex)->getSquare();
}

bool Bot::hasValidMove()
{
    return this->gameManager->board->hasValidPawnMoves(this->color, this->gameManager->getTurnContext()->getLastRolledValue());
}

Color Bot::getColor() const
{
    return color;
}

BroadcastingThread::BroadcastingThread(GameManager *gameManager, Response* response):
    QThread(nullptr)
{
    this->gameManager = gameManager;
    this->response = response;
}

void BroadcastingThread::run()
{
    this->gameManager->broadcastResponse(response);
    delete response;
}
